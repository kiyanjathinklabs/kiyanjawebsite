module.exports = {
    index:  (req, res) => {
        console.log("you've been routed to the index page.");
        //res.send('Welcome to the Nodejs Cms Tutorial');
        //res.render('default/index'), {layout: 'app.js'};
        res.render('default/index');
        
    },
    
  
    loginGet: (req, res) => {
        console.log("you've been routed to the loginGet action page.");
        //res.send("you've routed to the login page");
        //res.render('default/login'), {layout: 'app.js'};
        res.render('auth/login', {layout: 'login'});
    },

    loginPost: (req, res) => {
        console.log("you've been routed to the loginPost Action page.");
        //res.send("you've routed to the login page");
        //res.render('default/login'), {layout: 'app.js'};
        res.send('congratulation  login data successfully submitted');
    },

    registerGet: (req, res) => {
        console.log("you've been routed to the loginGet action page.");
        //res.send("you've routed to the login page");
        //res.render('default/login'), {layout: 'app.js'};
        res.render('auth/register', {layout: 'register'});
    },

    registerPost: (req, res) => {
        console.log("you've been routed to the loginPost Action page.");
        //res.send("you've routed to the login page");
        //res.render('default/login'), {layout: 'app.js'};
        res.send('congratulation  registration data successfully submitted');
    },

    adminGet: (req, res) => {
        console.log("you've been routed to the admin page.");
        //res.send("you've routed to the admin page");
        //res.render('default/admin'), {layout: 'app.js'};
        res.render('admin/admin',{layout: 'admin'});
    },

    adminPost: (req, res) => {
        console.log("you've been routed to the admin page.");
        //res.send("you've routed to the admin page");
        //res.render('default/admin'), {layout: 'app.js'};
        res.send('congratulation  admin data successfully submitted');
    }


};
