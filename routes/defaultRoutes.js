const express = require('express');
const router = express.Router();
const defaultController = require('../controllers/defaultController');
const adminController = require('../controllers/adminController');

router.all('/*', (req, res, next) => {
    
    req.app.locals.layout = 'default'

    next();
});
//Route for handling default incoming http request
router.route('/')
    .get(defaultController.index)
    

router.route('/login')
    .get(defaultController.loginGet)
    .post(defaultController.loginPost);
    
router.route('/register')
    .get(defaultController.registerGet)
    .post(defaultController.registerPost);

router.route('/admin')
    .get(adminController.index)
    //.post(defaultController.adminPost);

    


module.exports = router ;